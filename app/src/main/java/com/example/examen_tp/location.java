package com.example.examen_tp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class location extends AppCompatActivity {
    TextView nom, montant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        Intent intent = getIntent();
        String name = intent.getStringExtra("nom");
        String voiture = intent.getStringExtra("voiture");
        String nbr = intent.getStringExtra("nbr");

        nom = (TextView) findViewById(R.id.nomClient);
        montant = (TextView) findViewById(R.id.montant);

        nom.setText(name);
        montant.setText(String.valueOf(calculer(voiture, Integer.valueOf(nbr)))+ " dinars");
    }

    int calculer(String voiture, int nbr){
        int montant = calulMontant(voiture);
        return montant * nbr;
    }

    int calulMontant(String voiture){
        switch (voiture){
            case "Economique":
                return 50;
            case "Confort":
                return 100;
            case "Luxe":
                return 200;
        }
        return 0;
    }
}