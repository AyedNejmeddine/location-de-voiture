package com.example.examen_tp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    EditText nom, nbrjour;
    Button calculer;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nom = (EditText) findViewById(R.id.textNom);
        nbrjour = (EditText) findViewById(R.id.textNumberJour);
        calculer = (Button) findViewById(R.id.buttonCalculer);
        spinner = (Spinner) findViewById(R.id.spiner);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
                R.array.voitures, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        calculer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String n = String.valueOf(nom.getText());
                String nbr = String.valueOf(nbrjour.getText());
                String voiture = String.valueOf(spinner.getSelectedItem());
                if (n.isEmpty() || nbr.isEmpty()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                    alert.setTitle("Attention").setMessage("Il y'a l'un des champs est vide !")
                            .setPositiveButton("OK", null).show();
                } else {
                    Intent intent = new Intent(MainActivity.this, location.class);
                    intent.putExtra("nom", n).putExtra("nbr", nbr)
                            .putExtra("voiture", voiture);
                    startActivity(intent);
                }
            }
        });
    }
}